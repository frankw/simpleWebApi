from flask import render_template, flash, redirect, request, send_file, jsonify
from app import app
from .forms import SnKsnForm, SnListForm
import time,os,threading,json,xlwt

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/add_sn', methods=['GET', 'POST'])
def add_sn():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()
    form = SnKsnForm()
    status = ""
    # flash the list to exisiting page
    # for sn in SNs:
    #    flash(sn)

    # handle add new serial number
    if form.validate_on_submit():
        SNi = "%s\n" % form.sn.data.encode("ascii")
        print 'Submited = %s ' % SNi
        if SNi not in SNs:
            SNs.append(SNi)
            SNsFile.write(SNi)
            #createSheet(SNs)
            for sn in SNs:
                flash(sn)
            status = "Add success!"
        else:
            print 'Duplicate'
            status = "Add fails duplicate with exisiting DB!"

    # render the page
    return render_template('sn_input.html',
                           title='Input Serial Number',
                           form=form,
                           status=status)

@app.route('/add_sn_static', methods=['GET'])
def test():
    return app.send_static_file("sn_input_static.html");

@app.route('/list_sn_static', methods=['GET'])
def test2():
    return app.send_static_file("sn_input_list_static.html");

@app.route('/add_sn2', methods=['POST'])
def add_sn2():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()

    # retrieve json data from post request
    j = request.json
    SNi = "%s\n" % j['sn'].encode("ascii")

    # try insert to our DB
    if SNi not in SNs:
        SNs.append(SNi)
        SNsFile.write(SNi)
        SNsFile.close()
        return jsonify({"status":"success"})
    else:
        return jsonify({"status":"failure","cause":"Already Exist"})

@app.route('/list_sn2', methods=['GET'])
def list_sn2():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()

    # put sns into array
    SNlists = [];
    for sn in SNs:
       SNlists.append(sn.rstrip())
    print "SNlists = %s" %SNlists

    # create json data
    j = {"sn":SNlists}

    # return data
    return jsonify(j)

# start thread to generate xls file
@app.route('/gen_xls', methods=['POST'])
def gen_xls():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()
    t = threading.Thread(target=createSheet, args=(SNs,)) 
    t.start()
    return jsonify({"status":"success"})

@app.route('/delete_sn', methods=["POST"])
def delete_sn():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()

    # retrieve json data from post request
    j = request.json
    SNi = "%s\n" % j['sn'].encode("ascii")

    if SNi not in SNs:
        return jsonify({"status":"failure","cause":"Item not exist"})
    else:
        SNs.remove(SNi)
        SNsFile.seek(0)
        SNsFile.write(''.join(SNs))
        SNsFile.truncate()
        SNsFile.close()
        return jsonify({"status":"success"})

@app.route('/list_sn', methods=['GET',"POST"])
def list_sn():
    # retrieve from localDB
    SNsFile = open("./app/data/sn.txt","r+")
    SNs = SNsFile.readlines()
    form = SnListForm(request.form)
    form.SNs = SNs
    print form.SNs

    # handle form submitted event
    if form.is_submitted():
       print "submitted"
       item = request.form.get('item')
       action = request.form.get('action')
       if action is not None:
           print "action not none = %s " % action.encode("ascii").rstrip()
           if action.encode("ascii").rstrip() == "genXls":
               print "GenXls File"
               createSheet(SNs)
               flash("/data/sn.xls")
       if item is not None:
           item = item.encode("ascii").rstrip() + '\n'
           print "item in list = %s" % SNs.__contains__(item)
           if SNs.__contains__(item):
               SNs.remove(item)
               SNsFile.seek(0)
               SNsFile.write(''.join(SNs))
               SNsFile.truncate()
               SNsFile.close()

    # render the page
    return render_template('sn_input_list.html',form=form)

@app.route('/data/sn.xls', methods=['GET',"POST"])
def download():
    return send_file('./data/sn.xls', as_attachment=True)

# time comsuming task should run on thread
def createSheet(snList):
    # back up old files
    os.rename("./app/data/sn.xls", "./app/data/sn.xls.old")

    print "gen Excel Workbook"
    book = xlwt.Workbook()
    sheet1 = book.add_sheet("sn_ksn_import.csv")

    # row 1 - red color comment line
    row = sheet1.row(0)
    style = 'font: name Arial, color-index red;'
    row.write(0,"start with 3th line! don't be blank,only alphabet and number! only choose sn or ksn, not both!",xlwt.Style.easyxf(style))

    # row 2 - Table Header with column description
    row = sheet1.row(1)
    row.write(0,"SN")
    row.write(1,"KSN")

    # store list KSN into sheet
    rowNum = 2
    for SN in snList:
        row = sheet1.row(rowNum)
        row.write(0,SN.rstrip())
        rowNum += 1

    # save to file
    book.save("./app/data/sn.xls")
    print "gen Excel Workbook complete"
