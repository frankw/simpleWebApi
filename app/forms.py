from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired

class SnKsnForm(FlaskForm):
    sn = StringField('SerialNumber', validators=[DataRequired()])
    ksn = StringField('Key Serial Number')

class SnListForm(FlaskForm):
	SNs = []          # array SNs 
