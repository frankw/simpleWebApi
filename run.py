#!flask/bin/python
from app import app

from OpenSSL import SSL
 
import os
 
context = SSL.Context(SSL.SSLv23_METHOD)
cer = os.path.join(os.path.dirname(__file__), 'frank.crt')
key = os.path.join(os.path.dirname(__file__), 'frank.key')

context = (cer,key)
#app.run(host='0.0.0.0',port=8080,debug=True,ssl_context=context)
app.run(host='0.0.0.0',port=80,debug=True)
