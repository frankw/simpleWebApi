# What am I doing? 
This is my learning project to learning how to implemented restful api server and the web front UI.

# History and thought
**Skip it if you found it boring.**
I am embedded engineer for many years. I did low level programming on 8bit MCU to 32bit MUC with OS(Linux or uCOS) or without OS.
I write codes like drivers for accessing hardware via SPI, I2C, USB, UART, and middleware which will provide data to UI application.
I also did couple UI project base GTK but only very basic with few widgets. I did Android APKs for manufacture testing.
I found Android Java programming is very interesting.However,it isn't suitable for many embedded platform.

Since last year early 2016 (I relocated to new country and I start looking for new job), I realize my skill set for embedded engineering is not modernize enough.
It seems everywhere is looking skill to build IoT ,cloud connectivity ,Html 5 UI, restful API.
I see platform UI is develop with html 5 and control interface is implemented as backend restful API.
The UI logic need to program with javascript.

I did a bit research on it and I start to see beauty behind it.
 - http API method is generic, GET, POST , PUT and DELETE and it is synchronize
 - it is stateless
 - security can be protect by SSL if you need to access via internet connection
 - you can pass data using json and it is humman readable
 - javascript quite easy develop and debug with web browser
 - can implement many scalable UI using bootstrap
 - if want more responsive with mobile UI , you can easily implement native mobile app UI by using the restful API

There are many of advantages and modern embedded CPUs are powerful enough to support it.

# Plan 
I did some research online and chatted with some IT colleagues. I found that python + flask is easilest language to start.
So I can pay more focus on Front End UI. I also find that there is open source microsoft restful SDK project which I plan 
to migrate to after I get more familiar with the front end stuffs.

Stage #1
I use Miguel's Flask web form tutorial example as starting point. 
I'm implementing an inventory app which allow using to add/delete new Serial number of products to the inventory system.
I need to ensure the WebUI and WebAPI are seperated. The backend can replace with other implmentation with difference language.

Stage #2
Replace backend with C++ restful framework. It should be more suitable for embedded cpu.

Stage #3
Implement UI client use cpp and android

Stage #4
cross compile it on embedded platform 

Stage #5
take result #4 . Re-spin it to generic network appliance which use control and monitoring.
