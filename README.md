Simple python web app to add serial number and generate excel file.

Installation
------------
Few python package need to install

xlwt - generate excel file
flask - web framework

pip install xlwt
pip install flask

Running
-------

sudo python ./run.py


Add SSL Certificate
-------------------
https://udaraliyanage.wordpress.com/2015/10/21/python-flask-api-in-https/

Create self-signed ssl server certificate
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout frank.key -out frank.crt

Create pem file from key files
openssl rsa -in frank.key -text > frank.private.pem
openssl x509 -inform PEM -in frank.crt > frank.public.pem
